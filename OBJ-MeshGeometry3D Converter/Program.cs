﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Collections.ObjectModel;

namespace SUNRUSE.OBJMeshGeometry3DConverter
{

  internal static class Program
  {

    private struct Vertex
    {

      internal ReadOnlyCollection<Decimal> Position { get; set; }
      internal ReadOnlyCollection<Decimal> TextureCoordinate { get; set; }
      internal ReadOnlyCollection<Decimal> Normal { get; set; }
      
      public override int GetHashCode()
      {

        return GenerateHashCode( Position ) ^ GenerateHashCode( TextureCoordinate ) ^ GenerateHashCode( Normal );

      }

      private static int GenerateHashCode( ReadOnlyCollection<Decimal> from )
      {

        if( from == null )
          return 0;

        int output = 0;

        foreach( Decimal value in from )
          output ^= value.GetHashCode();

        return output;

      }

      public override Boolean Equals( Object obj )
      {

        if( !( obj is Vertex ) )
          return false;

        Vertex other = ( Vertex ) obj;

        if( !ArraysEqual( Position, other.Position ) )
          return false;

        if( !ArraysEqual( TextureCoordinate, other.TextureCoordinate ) )
          return false;

        if( !ArraysEqual( Normal, other.Normal ) )
          return false;

        return true;

      }

      private static Boolean ArraysEqual( ReadOnlyCollection<Decimal> a, ReadOnlyCollection<Decimal> b )
      {

        if( a == null && b == null )
          return true;

        if( a == null || b == null )
          return false;

        if( a.Count != b.Count )
          return false;

        return a.Zip( b, ( aElement, bElement ) => aElement == bElement ).All( value => value );

      }
      
    }

    private sealed class OBJParseException : Exception
    {

      internal OBJParseException( String message )
        : base( message )
      {
      }

    }

    [STAThread]
    private static void Main( string[] args )
    {

      String input = null;

      if( args != null && args.Length >= 1 )
        input = args[ 0 ];

      if( String.IsNullOrWhiteSpace( input ) )
      {

        OpenFileDialog open = new OpenFileDialog();

        open.Title = "Choose a Lightwave/OBJ file to convert";
        open.Filter = "Lightwave/OBJ (*.obj)|*.obj";

        if( open.ShowDialog() != DialogResult.OK )
          return;

        input = open.FileName;

      }

      String output = null;

      if( args != null && args.Length >= 2 )
        output = args[ 1 ];

      if( String.IsNullOrWhiteSpace( output ) )
      {

        SaveFileDialog save = new SaveFileDialog();

        save.Title = "Choose where to save the created MeshGeometry3D";
        save.Filter = "XAML Static Resource (*.xaml)|*.xaml";

        const String extension = ".obj";

        save.FileName = input.EndsWith( extension, StringComparison.InvariantCultureIgnoreCase ) ? input.Substring( 0, input.Length - extension.Length ) + ".xaml" : input;

        if( save.ShowDialog() != DialogResult.OK )
          return;

        output = save.FileName;

      }

      Console.WriteLine( "Reading \"{0}\"...", input );

      try
      {

        List<KeyValuePair<String, ReadOnlyCollection<ReadOnlyCollection<Vertex>>>> meshes = new List<KeyValuePair<String, ReadOnlyCollection<ReadOnlyCollection<Vertex>>>>();

        using( StreamReader reader = new StreamReader( input ) )
        {

          List<ReadOnlyCollection<Decimal>> positions = new List<ReadOnlyCollection<Decimal>>();
          List<ReadOnlyCollection<Decimal>> textureCoordinates = new List<ReadOnlyCollection<Decimal>>();
          List<ReadOnlyCollection<Decimal>> normals = new List<ReadOnlyCollection<Decimal>>();

          String meshName = null;
          List<ReadOnlyCollection<Vertex>> faces = new List<ReadOnlyCollection<Vertex>>();

          String line = null;

          while( ( line = reader.ReadLine() ) != null )
          {

            if( String.IsNullOrWhiteSpace( line ) )
              continue;

            String[] segments = line.Split( null );

            switch( segments[ 0 ] )
            {

              case "v":

                if( segments.Length == 1 )
                  throw new OBJParseException( "Vertices must define at least one dimension" );

                try
                {

                  positions.Add( segments.Skip( 1 ).Select( value => Decimal.Parse( value ) ).ToList().AsReadOnly() );

                }
                catch( FormatException )
                {

                  throw new OBJParseException( "Vertices must be followed by a sequence of valid decimals until the end of the line" );

                }

                break;

              case "vt":

                if( segments.Length == 1 )
                  throw new OBJParseException( "Texture coordinates must define at least one dimension" );

                try
                {

                  List<Decimal> coordinate = segments.Skip( 1 ).Select( value => Decimal.Parse( value ) ).ToList();

                  if( coordinate.Count >= 2 )
                    coordinate[ 1 ] = 1.0m - coordinate[ 1 ];

                  textureCoordinates.Add( coordinate.AsReadOnly() );

                }
                catch( FormatException )
                {

                  throw new OBJParseException( "Texture coordinates must be followed by a sequence of valid decimals until the end of the line" );

                }

                break;

              case "vn":

                if( segments.Length == 1 )
                  throw new OBJParseException( "Normals must define at least one dimension" );

                try
                {

                  normals.Add( segments.Skip( 1 )
                    .Select( value => Decimal.Parse( value ) ).ToList().AsReadOnly() );

                }
                catch( FormatException )
                {

                  throw new OBJParseException( "Normals must be followed by a sequence of valid decimals until the end of the line" );

                }

                break;

              case "f":

                if( segments.Length < 4 )
                  throw new OBJParseException( "Faces must define at least three vertices" );

                List<Vertex> face = new List<Vertex>();

                foreach( String vertexString in segments.Skip( 1 ) )
                {

                  Vertex vertex = new Vertex();

                  int elementID = 0;

                  foreach( String element in vertexString.Split( '/' ) )
                  {

                    if( elementID == 3 )
                      throw new OBJParseException( "Only vertices, texture coordinates and normals are supported; failed to parse as a vertex specified four elements" );

                    int? index = ( int? ) null;

                    if( !String.IsNullOrWhiteSpace( element ) )
                    {

                      int ourIndex = 0;

                      if( !int.TryParse( element, out ourIndex ) )
                        throw new OBJParseException( "Face vertices must contain valid integers separated by forwards slash characters" );

                      index = ourIndex;

                    }

                    if( index == 0 )
                      throw new OBJParseException( "The OBJ specification does not define how face vertices with an index of zero should be handled" );

                    List<ReadOnlyCollection<Decimal>> relevantList = null;

                    switch( elementID )
                    {

                      case 0:
                        relevantList = positions;
                        break;

                      case 1:
                        relevantList = textureCoordinates;
                        break;

                      case 2:
                        relevantList = normals;
                        break;

                      default:
                        throw new NotImplementedException();

                    }

                    ReadOnlyCollection<Decimal> data = null;

                    if( index.HasValue )
                      if( index.Value > 0 )
                      {

                        if( index > relevantList.Count )
                          throw new OBJParseException( "Positive face vertex index out of range" );

                        data = relevantList[ index.Value - 1 ];

                      }
                      else
                      {

                        if( ( -index ) > relevantList.Count )
                          throw new OBJParseException( "Negative face vertex index out of range" );

                        data = relevantList[ relevantList.Count + index.Value ];

                      }

                    switch( elementID++ )
                    {

                      case 0:
                        vertex.Position = data;
                        break;

                      case 1:
                        vertex.TextureCoordinate = data;
                        break;

                      case 2:
                        vertex.Normal = data;
                        break;

                      default:
                        throw new NotImplementedException();

                    }

                  }

                  face.Add( vertex );

                }

                // Triangulate faces.
                if( face.Count != 3 )
                  faces.AddRange( face.Skip( 1 ).Zip( face.Skip( 2 ), ( a, b ) => new List<Vertex> { face.First(), a, b }.AsReadOnly() ) );
                else
                  faces.Add( face.AsReadOnly() );

                break;

              case "o":

                if( segments.Length != 2 )
                  throw new OBJParseException( "Objects must have a name containing no whitespace" );

                // We might name an object right from the start.
                if( meshName != null && !faces.Any() )
                  throw new OBJParseException( "Objects must contain faces" );

                if( faces.Any() )
                  meshes.Add( new KeyValuePair<string, ReadOnlyCollection<ReadOnlyCollection<Vertex>>>( meshName, faces.AsReadOnly() ) );

                meshName = segments[ 1 ];
                faces = new List<ReadOnlyCollection<Vertex>>();

                break;

              default:
                Console.WriteLine( "Skipping unknown line \"{0}\"...", line );
                break;

            }

          }

          Console.WriteLine( "Found {0} vertices, {1} texture coordinates, {2} normals and {3} faces in {5}. ({4} total indices)", positions.Count, textureCoordinates.Count, normals.Count, faces.Count, faces.Sum( face => face.Count ), meshes.Count + 1 );

          if( !faces.Any() )
            throw new OBJParseException( "Objects must contain faces" );

          meshes.Add( new KeyValuePair<string, ReadOnlyCollection<ReadOnlyCollection<Vertex>>>( meshName, faces.AsReadOnly() ) );

          if( !positions.Any() )
            throw new OBJParseException( "No vertices were found in the file" );

        }

        if( meshes.Select( mesh => mesh.Key ).Distinct().Count() != meshes.Count )
          throw new OBJParseException( "Object names must be unique" );

        Console.WriteLine( "Writing to \"{0}\"...", output );

        using( StreamWriter writer = new StreamWriter( output ) )
        {

          writer.WriteLine( "<ResourceDictionary" );
          writer.WriteLine( "\txmlns=\"http://schemas.microsoft.com/winfx/2006/xaml/presentation\"" );
          writer.WriteLine( "\txmlns:x=\"http://schemas.microsoft.com/winfx/2006/xaml\">" );

          foreach( KeyValuePair<String, ReadOnlyCollection<ReadOnlyCollection<Vertex>>> mesh in meshes )
          {

            ReadOnlyCollection<Vertex> vertices = mesh.Value.SelectMany( face => face ).Distinct().ToList().AsReadOnly();

            Console.WriteLine( "Writing object \"{0}\" ({1} indices, {2} vertices)", mesh.Key, vertices.Count, mesh.Value.Count );

            writer.WriteLine( "\t<MeshGeometry3D x:Key=\"{0}\">", mesh.Key );


            if( vertices.Any( vertex => vertex.Position != null && vertex.Position.Any() ) )
            {

              writer.WriteLine( "\t\t<MeshGeometry3D.Positions>" );

              foreach( Vertex vertex in vertices )
                if( vertex.Position != null && vertex.Position.Any() )
                  writer.WriteLine( "\t\t\t{0}", String.Join( ", ", vertex.Position ) );
                else
                  writer.WriteLine( "\t\t\t0" );

              writer.WriteLine( "\t\t</MeshGeometry3D.Positions>" );

            }


            if( vertices.Any( vertex => vertex.TextureCoordinate != null && vertex.TextureCoordinate.Any() ) )
            {

              writer.WriteLine( "\t\t<MeshGeometry3D.TextureCoordinates>" );

              foreach( Vertex vertex in vertices )
                if( vertex.TextureCoordinate != null && vertex.TextureCoordinate.Any() )
                  writer.WriteLine( "\t\t\t{0}", String.Join( ", ", vertex.TextureCoordinate ) );
                else
                  writer.WriteLine( "\t\t\t0" );

              writer.WriteLine( "\t\t</MeshGeometry3D.TextureCoordinates>" );

            }


            if( vertices.Any( vertex => vertex.Normal != null && vertex.Normal.Any() ) )
            {

              writer.WriteLine( "\t\t<MeshGeometry3D.Normals>" );

              foreach( Vertex vertex in vertices )
                if( vertex.Normal != null && vertex.Normal.Any() )
                  writer.WriteLine( "\t\t\t{0}", String.Join( ", ", vertex.Normal ) );
                else
                  writer.WriteLine( "\t\t\t0" );

              writer.WriteLine( "\t\t</MeshGeometry3D.Normals>" );

            }


            writer.WriteLine( "\t\t<MeshGeometry3D.TriangleIndices>" );

            foreach( ReadOnlyCollection<Vertex> face in mesh.Value )
              writer.WriteLine( "\t\t\t{0}", String.Join( " ", face.Select( vertex => vertices.IndexOf( vertex ) ) ) );

            writer.WriteLine( "\t\t</MeshGeometry3D.TriangleIndices>" );


            writer.WriteLine( "\t</MeshGeometry3D>" );

          }

          writer.WriteLine( "</ResourceDictionary>" );

        }

      }
      catch( OBJParseException exception )
      {

        MessageBox.Show( exception.Message + ".", String.Format( "Error parsing OBJ \"{0}\"", input ), MessageBoxButtons.OK, MessageBoxIcon.Stop );
        throw;

      }
      catch( Exception exception )
      {

        MessageBox.Show( exception.ToString(), "Unexpected exception", MessageBoxButtons.OK, MessageBoxIcon.Stop );
        throw;

      }

    }

  }

}
